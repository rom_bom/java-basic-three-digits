import java.util.Scanner;

public class ThreeDigits {
    public static void main(String[] args) {

        int[] myArray;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of digits : "); // вводимо кількість цифр
        n = in.nextInt();
        myArray = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Enter digit[" + i + "] = ");
            myArray[i] = in.nextInt();
        }

        int max = Integer.MIN_VALUE; // оголошуємо змінну max, присвоюємо їй мінімальне значення
        for (int i = 0; i < myArray.length; i++) { // перебираємо усі елементи масиву
            max = Math.max(max, myArray[i]); // змінній max присвоюємо перше максимальне значення та порівнюємо усі елементи з ним
        }
        System.out.println("The largest number is: " + max); // друкуємо найбільше число
    }
}
